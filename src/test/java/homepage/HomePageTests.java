package homepage;

import  static org.hamcrest.MatcherAssert.assertThat;
import  static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import base.BaseTests;
import pages.LoginPage;
import pages.ModalProdutoPage;
import pages.ProdutoPage; 

public class HomePageTests extends BaseTests {
	
	@Test 
	public void testcontrarProdutos_oitoProdutosDiferentes() {
		carregarPaginaInicial(); 
		assertThat(homePage.contarProdutos(), is(8)); 
		
	}
	
	@Test
	public void testValidarCarrinhoZerado_ZeroItensNoCarrinho() {
		
		int produtosNoCarrinho = homePage.obterQuantidadeProdutosNoCarrinho(); 
		assertThat(produtosNoCarrinho, is(0));
	}
	
	ProdutoPage produtoPage;
	@Test 
	public void testValidarDetalhesDoProduto_DescricaoEValorIguais() {
		
		int indice = 0 ; 
		String nomeProduto_HomePage = homePage.obterNomeProduto(indice); 
		String precoProduto_HomePage = homePage.obterPrecoProduto(indice); 
		System.out.println(nomeProduto_HomePage);
		System.out.println(precoProduto_HomePage);
		
		produtoPage = homePage.ClicarProduto(indice);
		
		String nomeProduto_ProdutoPage = produtoPage.obterNomeProduto(); 
		String precoProduto_ProdutoPage = produtoPage.obterPrecoProduto(); 
		System.out.println(nomeProduto_ProdutoPage);
		System.out.println(precoProduto_ProdutoPage);
		
		assertThat(nomeProduto_HomePage.toUpperCase(), is(nomeProduto_ProdutoPage.toUpperCase()));
		assertThat(precoProduto_ProdutoPage, is(precoProduto_ProdutoPage));
		
	}
	
	LoginPage loginPage ;
	@Test 
	public void testLoginComSucesso_UsuarioLogado() {
		//Clicar no botão Sing in na Home Page 
		loginPage = homePage.clicarBotaoSingIn(); 
	
		// Preencher usuário e senha 
		loginPage.preencherEmail("romario@teste.com");
		loginPage.preencherPassword("123456");
		
		
		//Clicar no botão Sing in para logar 
		loginPage.clicarNoBotaoSingIn();
		
		// Validar se o usuário está Logado de fato 
		assertThat(homePage.estaLogado("romario silva"), is(true));
		
		carregarPaginaInicial();
		
		
	}
	
	@Test 
	public void testIncluirPordutoNoCarrinho_ProdutoIncluidoComSucesso () {
		
		String tamanhoProduto = "M"; 
		String corPerta = "Black" ; 
		int quantidadeProduto = 2;
		//-- pré-codição 
		if (!homePage.estaLogado("romario silva")) {
			testLoginComSucesso_UsuarioLogado();
		}
		//--Teste
		//Selecionado produto
		testValidarDetalhesDoProduto_DescricaoEValorIguais() ;
		
		//Selecionar Tamanho 
		List<String> listaOpcoes =   produtoPage.obterOpcoesSelecionadas();
		
		System.out.println(listaOpcoes.get(0));
		System.out.println("Tamanho da Lista " + listaOpcoes.size());
		
		produtoPage.selecionarOpcaoDropDown(tamanhoProduto);
		
		listaOpcoes =   produtoPage.obterOpcoesSelecionadas();
		
		System.out.println(listaOpcoes.get(0));
		System.out.println("Tamanho da Lista " + listaOpcoes.size());
		// selecionar cor
		produtoPage.selecionarCorPreta();
		
		//Selecionar Quantidade 
		produtoPage.alterarQuantidade(quantidadeProduto);
		
		//	Adicionar no carrinho
				ModalProdutoPage modalProdutoPage = produtoPage.clicarBotaoAddToCart(); 
		
		assertTrue(modalProdutoPage.obterMensagemProdutoAdicionado().endsWith("Product successfully added to your shopping cart"));

	
	}

}
